<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
// Start Add +

    //Stores
    Route::resource('/store', App\Http\Controllers\Admin\StoresController::class);

    //Departments
    Route::resource('/departments', App\Http\Controllers\Admin\DepartmentsController::class);

    //Page Names
    Route::resource('/pages', App\Http\Controllers\Admin\PagesController::class);

    //Employees
    Route::resource('/employees', App\Http\Controllers\Admin\EmployeesController::class);

    //Campaigns
    Route::resource('/campaigns', App\Http\Controllers\Admin\CampaignController::class);

    //Creative Types
    Route::resource('/creative-types', App\Http\Controllers\Admin\CreativeTypeController::class);

    // Strategies
    Route::resource('/strategies', App\Http\Controllers\Admin\StrategyController::class);

    // Status
    Route::resource('/status', App\Http\Controllers\Admin\StatusController::class);

    // Ads Type
    Route::resource('/ads-type', App\Http\Controllers\Admin\AdsTypeController::class);

    // Ads Strategy
    Route::resource('/ads-strategy', App\Http\Controllers\Admin\AdsStrategyController::class);

    // Agent Teams
    Route::resource('/agent-teams', App\Http\Controllers\Admin\AgentTeamsController::class);

// End Add +


// Centralized
Route::resource('/centralized', App\Http\Controllers\All\CentralizeController::class);
Route::get('/running', [App\Http\Controllers\All\CentralizeController::class, 'running'])->name('centralized.running');
Route::get('/killed', [App\Http\Controllers\All\CentralizeController::class, 'killed'])->name('centralized.killed');

// Trackers
Route::get('/budget-per-product', [App\Http\Controllers\TrackersController::class, 'budgetPerProducts'])->name('budget-per-product.budgetPerProducts');
Route::get('/budget-per-product/{product_name}', [App\Http\Controllers\TrackersController::class, 'showBudgetPerProducts'])->name('budget-per-product.showBudgetPerProducts');