<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AdsStrategiesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ad_strategies')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'ads_strategy' => 'Regular Ads',
                ),
            1 =>
                array (
                    'id' => 2,
                    'ads_strategy' => 'Convert',
                ),
            2 =>
                array (
                    'id' => 3,
                    'ads_strategy' => 'Existing Ads',
                ),
            3 =>
                array (
                    'id' => 4,
                    'ads_strategy' => 'Replace Ads',
                ),
            4 =>
                array (
                    'id' => 5,
                    'ads_strategy' => 'Additional Ads',
                ),
            5 => 
                array (
                    'id' => 6,
                    'ads_strategy' => 'Bullet',
                ),
            6 =>
                array (
                    'id' => 7,
                    'ads_strategy' => 'LLA',
                ),
            7 =>
                array (
                    'id' => 8,
                    'ads_strategy' => 'Sponsored',
                ),
            8 =>
                array (
                    'id' => 9,
                    'ads_strategy' => 'Retarget',
                ),
            ));
    }
}
