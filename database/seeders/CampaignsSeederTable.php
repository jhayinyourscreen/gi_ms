<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CampaignsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaigns')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'campaign_name' => 'Messaging',
                    'campaign_code' => 'MSG',
                ),
            1 =>
                array (
                    'id' => 2,
                    'campaign_name' => 'Purchasing',
                    'campaign_code' => 'PUR',
                ),
            ));
    }
}
