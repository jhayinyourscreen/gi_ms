<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DepartmentsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'department_name' => 'Product Research',
                ),
            1 =>
                array (
                    'id' => 2,
                    'department_name' => 'Creatives',
                ),
            2 =>
                array (
                    'id' => 3,
                    'department_name' => 'Advertiser',
                ),
            3 =>
                array (
                    'id' => 4,
                    'department_name' => 'Marketing Head',
                ),
            ));
    }
}
