<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StrategiesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('strategies')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'strategy_name' => 'New',
                ),
            1 =>
                array (
                    'id' => 2,
                    'strategy_name' => 'Old',
                ),
            ));
    }
}
