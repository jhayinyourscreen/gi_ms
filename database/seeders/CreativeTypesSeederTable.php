<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CreativeTypesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('creative_types')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'creative_types' => 'Video 1',
                ),
            1 =>
                array (
                    'id' => 2,
                    'creative_types' => 'Video 2',
                ),
            2 =>
                array (
                    'id' => 3,
                    'creative_types' => 'Static',
                ),
            3 =>
                array (
                    'id' => 4,
                    'creative_types' => 'Reviews',
                ),
            4 =>
                array (
                    'id' => 5,
                    'creative_types' => 'Catalogue',
                ),
            ));
    }
}
