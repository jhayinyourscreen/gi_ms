<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AdsTypesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ad_types')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'ads_type' => 'New',
                ),
            1 =>
                array (
                    'id' => 2,
                    'ads_type' => 'Old',
                ),
            2 =>
                array (
                    'id' => 3,
                    'ads_type' => 'Recreate',
                ),
            3 =>
                array (
                    'id' => 4,
                    'ads_type' => 'Rerun',
                ),
            ));
    }
}
