<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use DB;

class UsersSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'f_name' => 'Green',
            'l_name' => 'Ink',
            'email' => 'greenink@gmail.com',
            'password' => Hash::make('greenink123'),
            'image' => 'default.jpg',
        ]);
    }
}
