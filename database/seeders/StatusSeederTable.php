<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StatusSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'status' => 'Running',
                ),
            1 =>
                array (
                    'id' => 2,
                    'status' => 'Paused',
                ),
            2 =>
                array (
                    'id' => 3,
                    'status' => 'Rejected',
                ),
            3 =>
                array (
                    'id' => 4,
                    'status' => 'Disabled',
                ),
            4 =>
                array (
                    'id' => 5,
                    'status' => 'Killed',
                ),
        ));
    }
}
