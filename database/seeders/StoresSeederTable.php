<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StoresSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'store_name' => 'Pop and Click',
                    'store_code' => 'PNC',
                ),
            1 =>
                array(
                    'id' => 2,
                    'store_name' => 'Health is Wealth',
                    'store_code' => 'HIW',
                ),
            2 =>
                array(
                    'id' => 3,
                    'store_name' => 'ATM Watches',
                    'store_code' => 'ATM',
                ),
            ));
    }
}
