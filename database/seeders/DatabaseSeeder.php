<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersSeederTable::class,
            StatusSeederTable::class,
            DepartmentsSeederTable::class,
            CampaignsSeederTable::class,
            StrategiesSeederTable::class,
            CreativeTypesSeederTable::class,
            AdsStrategiesSeederTable::class,
            AdsTypesSeederTable::class,
            TeamSeederTable::class,
            StoresSeederTable::class,
        ]);
    }
}
