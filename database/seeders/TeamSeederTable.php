<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TeamSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agent_teams')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'agent_team' => 'Amanpulo',
                ),
            1 =>
                array (
                    'id' => 2,
                    'agent_team' => 'Boracay',
                ),
            2 =>
                array (
                    'id' => 3,
                    'agent_team' => 'Coron',
                ),
            ));
    }
}
