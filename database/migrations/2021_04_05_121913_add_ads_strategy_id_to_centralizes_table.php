<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdsStrategyIdToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->bigInteger('ads_strategy_id')->unsigned()->nullable()->after('ads_type_id');
            $table->foreign('ads_strategy_id')->references('id')->on('ad_strategies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropForeign(['ads_strategy_id']);
            $table->dropColumn('ads_strategy_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
