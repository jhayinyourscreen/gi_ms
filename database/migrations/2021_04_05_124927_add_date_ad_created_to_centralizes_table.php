<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateAdCreatedToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->date('date_ad_created')->nullable()->after('ads_strategy_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropColumn('date_ad_created');
        });
    }
}
