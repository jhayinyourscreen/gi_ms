<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreativeTypeIdToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->bigInteger('creative_type_id')->unsigned()->nullable()->after('status_id');
            $table->foreign('creative_type_id')->references('id')->on('creative_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropForeign(['creative_type_id']);
            $table->dropColumn('creative_type_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
