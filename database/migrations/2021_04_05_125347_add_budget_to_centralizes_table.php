<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBudgetToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->integer('budget')->unsigned()->nullable()->after('date_ads_run');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropColumn('budget');
        });
    }
}
