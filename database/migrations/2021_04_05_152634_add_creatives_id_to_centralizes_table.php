<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreativesIdToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->bigInteger('creatives_id')->unsigned()->nullable()->after('creative_type_id');
            $table->foreign('creatives_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropForeign(['creatives_id']);
            $table->dropColumn('creatives_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
