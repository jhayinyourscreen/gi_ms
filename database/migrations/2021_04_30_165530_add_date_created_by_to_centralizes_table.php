<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateCreatedByToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->bigInteger('sequence_created_by')->unsigned()->nullable();
            $table->foreign('sequence_created_by')->references('id')->on('users');
            $table->date('date_sequence_created')->nullable();
            $table->bigInteger('json_created_by')->unsigned()->nullable();
            $table->foreign('json_created_by')->references('id')->on('users');
            $table->date('date_json_created')->nullable();
            $table->date('date_ads_paused')->nullable();
            $table->date('date_ads_rejected')->nullable();
            $table->date('date_ads_disabled')->nullable();
            $table->text('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::disableForeignKeyConstraints();

        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropForeign(['sequence_created_by']);
            $table->dropColumn('sequence_created_by');
            $table->dropForeign(['json_created_by']);
            $table->dropColumn('json_created_by');
            $table->dropColumn('date_sequence_created');
            $table->dropColumn('date_ads_paused');
            $table->dropColumn('date_ads_rejected');
            $table->dropColumn('date_ads_disabled');
            $table->dropColumn('notes');
            $table->dropColumn('date_json_created');
        });

        Schema::enableForeignKeyConstraints();
    }
}
