<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateAdsRunToCentralizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->date('date_ads_run')->nullable()->after('date_ad_created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('centralizes', function (Blueprint $table) {
            $table->dropColumn('date_ads_run');
        });
    }
}
