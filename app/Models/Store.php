<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;

    protected $fillable = [
        'store_name',
        'store_code',
    ];

    public function pages()
    {
        return $this->belongsTo('Pages', 'store_id');
    }

    public function teams()
    {
        return $this->belongsTo('App\Models\AgentTeam', 'team_id');
    }
}
