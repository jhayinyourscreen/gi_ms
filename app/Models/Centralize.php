<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Centralize extends Model
{

    protected $table = 'centralizes';
    protected $fillable = [
        'product_name',
        'creatives_id',

    ];
    protected $dates = [
        'date_creative_created',
        'date_ads_run',
        'date_ad_created',
        'date_ads_killed',
        'date_sequence_created',
        'date_ads_paused',
        'date_ads_rejected',
        'date_ads_disabled',
        'date_json_created',
    ];

    protected $casts = [
        'date_ad_created' => 'date:Y-d-m',
        'date_ads_run' => 'date:Y-d-m',
        'date_creative_created' => 'date:Y-d-m',
        'date_ads_killed' => 'date:Y-d-m',
        'date_sequence_created' => 'date:Y-d-m',
        'date_ads_paused' => 'date:Y-d-m',
        'date_ads_rejected' => 'date:Y-d-m',
        'date_ads_disabled' => 'date:Y-d-m',
        'date_json_created' => 'date:Y-d-m',
    ];

    use HasFactory;

    public function stores()
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }

    public function campaigns()
    {
        return $this->belongsTo('App\Models\Campaign', 'campaign_id');
    }

    public function strategies()
    {
        return $this->belongsTo('App\Models\Strategy', 'strategy_id');
    }

    public function statuses()
    {
        return $this->belongsTo('App\Models\Status', 'status_id');
    }

    public function pages()
    {
        return $this->belongsTo('App\Models\Page','page_id');
    }

    public function creative_types()
    {
        return $this->belongsTo('App\Models\CreativeType', 'creative_type_id');
    }

    public function adstypes()
    {
        return $this->belongsTo('App\Models\AdType','ads_type_id');
    }

    public function adsstrategies()
    {
        return $this->belongsTo('App\Models\AdStrategy','ads_strategy_id');
    }

    public function creatives()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function teams()
    {
        return $this->belongsTo('App\Models\AgentTeam');
    }
    
    public function adsteam()
    {
        return $this->belongsTo('App\Models\User', 'ad_created_by');
    }

    public function sequences()
    {
        return $this->belongsTo('App\Models\User', 'sequence_created_by');
    }

    public function json()
    {
        return $this->belongsTo('App\Models\User', 'json_created_by');
    }
}
