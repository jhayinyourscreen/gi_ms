<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $fillable = [
        'page_name',
        'store_id',
        'links',
    ];
    
    use HasFactory;

    public function stores()
    {
        return $this->belongsTo('App\Models\Store', 'store_id');
    }
}
