<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Page;
use App\Models\Store;

use Session;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        $pages = Page::all();
        return view('admin.pages.index', compact('stores', 'pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_id' => 'required',
            'page_name' => 'required|unique:pages',
            'links' => 'required',
        ]);

        $pages = new Page;
        $pages->page_name = $request->page_name;
        $pages->store_id = $request->store_id;
        $pages->links = $request->links;

        $pages->save();

        Session::flash('success', 'Pages was Created Succesfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
        [
            'store_id' => 'required',
            'page_name' => 'required|unique:pages',
            'links' => 'required',
        ]);

        $pages = Page::find($id);
        $pages->store_id = $request->store_id;
        $pages->page_name = $request->page_name;
        $pages->links = $request->links;

        $pages->save();
        Session::flash('success', 'Page was Updated Successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pages = Page::find($id);
        $pages->delete();

        Session::flash('success', 'Page was succesfully deleted!');
        return redirect()->back();
    }
}
