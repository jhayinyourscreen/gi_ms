<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Store;
use App\Models\AgentTeam;

use Session;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        $teams = AgentTeam::all();
        return view('admin.store.index', compact('stores', 'teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_name' => 'required|unique:stores',
            'store_code' => 'required|unique:stores',
        ]);

        $stores = new Store;
        $stores->store_name = $request->store_name;
        $stores->store_code = $request->store_code;
        $stores->team_id = $request->team_id;
        $stores->save();

        Session::flash('success', 'Store was Created Successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stores = Store::find($id);
        return view('admin.store.show', compact('stores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
        [
            'store_name' => 'required',
            'store_code' => 'required',
        ]);

        $stores = Store::find($id);
        $stores->store_name = $request->store_name;
        $stores->store_code = $request->store_code;
        $stores->team_id = $request->team_id;
        $stores->save();

        Session::flash('success', 'Store was Updated Successfully!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stores = Store::find($id);
        $stores->delete();
        
        Session::flash('success', 'Store was deleted successfully!');
        return redirect()->back();
    }
}
