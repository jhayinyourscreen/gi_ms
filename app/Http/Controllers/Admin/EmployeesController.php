<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Department;
use DB;

use Illuminate\Support\Facades\Hash;

use Session;
use Carbon\Carbon;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $employees = User::all();

        return view('admin.employees.index',compact('departments','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
            'department_id' => 'required',
            'is_team_leader' => 'required',
        ]);

        $data = $request->all();

        $employees = new User;
        $employees->f_name = $data['f_name'];
        $employees->l_name = $data['l_name'];
        $employees->email = $data['email'];
        $employees->password = Hash::make($data['password']);
        $employees->department_id = $data['department_id'];
        $employees->image = 'default.jpg';
        $employees->is_team_leader = $data['is_team_leader'];
        $employees->save();

        Session::flash('success', 'Employee was created succesfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
