<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Models\Centralize;
use App\Models\Store;
use DB;

class TrackersController extends Controller
{
    public function budgetPerProducts()
    {
        // $budgetproducts = Centralize::select('product_name','store_id')->groupBy('product_name')->groupBy('store_id')->get();
        $budgetproducts = Centralize::select(['product_name',
        \DB::raw('SUM(budget) as budget'),
        \DB::raw('COUNT(product_name) as total_products')])
        ->whereNotNull('date_ads_run')
        ->whereNotNull('campaign_id')
        ->where('status_id','=',1)
        ->groupBy('product_name')
        ->orderBy('product_name','asc')
        ->get();

        return view('trackers.budget-per-product', compact('budgetproducts'));
    }

    public function showBudgetPerProducts($product_name)
    {
        $showproducts = Centralize::select(['id','product_name','store_id','campaign_id','budget','date_ads_run'])
        ->whereNotNull('date_ads_run')
        ->whereNotNull('campaign_id')
        ->where('product_name','=',$product_name)
        ->where('status_id','=',1)
        ->orderBy('date_ads_run','asc')
        ->paginate(10);

        // $showstores = Centralize::select(['store_id','budget','campaign_id','status_id','date_ads_run',
        // \DB::raw('SUM(budget as budget)')])
        // ->whereNotNull('date_ads_run')
        // ->whereNotNull('status_id')
        // ->whereNotNull('campaign_id')
        // ->where('status_id','=',1)
        // ->groupBy('status_id')
        // ->orderBy('store_id', 'asc')
        // ->get();
        // dd($showstores);

        $showstores = Centralize::select(['store_id',
        \DB::raw('SUM(budget) as budget'),
        \DB::raw('COUNT(product_name) as total_products')])
        ->whereNotNull('date_ads_run')
        ->whereNotNull('campaign_id')
        ->where('product_name','=',$product_name)
        ->where('status_id','=',1)
        ->groupBy('store_id')
        ->orderBy('store_id','asc')
        ->get();
        

        return view('trackers.show.budget-per-product', compact('showproducts','showstores'));
    }
}
