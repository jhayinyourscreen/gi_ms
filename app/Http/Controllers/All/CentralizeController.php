<?php

namespace App\Http\Controllers\All;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Store;
use App\Models\Campaign;
use App\Models\Strategy;
use App\Models\Status;
use App\Models\Page;
use App\Models\CreativeType;
use App\Models\AdStrategy;
use App\Models\AdType;
use App\Models\User;
use App\Models\Centralize;
use App\Models\AgentTeam;

use Session;


class CentralizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centralizes = Centralize::all();
        $teams = AgentTeam::all();
        $stores = Store::all();
        $campaigns = Campaign::all();
        $strategies = Strategy::all();
        $statuses = Status::all();
        $pages = Page::all();
        $creative_types = CreativeType::all();
        $adsstrategies = AdStrategy::all();
        $adstypes = AdType::all();
        $creatives = User::where('department_id','=','2')->get();
        $adsteam = User::where('department_id','=','3')->get();
        return view('centralized.all', compact('stores','campaigns','strategies','statuses', 'pages','creative_types','adsstrategies','adstypes','creatives','centralizes','adsteam','teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $centralizes = Centralize::all();
        $teams = AgentTeam::all();
        $stores = Store::all();
        $campaigns = Campaign::all();
        $strategies = Strategy::all();
        $statuses = Status::all();
        $pages = Page::all();
        $creative_types = CreativeType::all();
        $adsstrategies = AdStrategy::all();
        $adstypes = AdType::all();
        $creatives = User::where('department_id','=','2')->get();
        $adsteam = User::where('department_id','=','3')->get();
        return view('centralized.create', compact('stores','campaigns','strategies','statuses', 'pages','creative_types','adsstrategies','adstypes','creatives','centralizes','adsteam','teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'store_id' => 'required',
        ]);

        $data = $request->all();
        
        $centralizes = new Centralize;
        $centralizes->product_name = $data['product_name'];
        $centralizes->store_id = $data['store_id'];
        $centralizes->page_id = $request->page_id;
        $centralizes->post_id = $request->post_id;
        $centralizes->ads_name = $request->ads_name;
        $centralizes->campaign_id = $request->campaign_id;
        $centralizes->strategy_id = $request->strategy_id;
        $centralizes->ads_strategy_id = $request->ads_strategy_id;
        $centralizes->ads_type_id = $request->ads_type_id;
        $centralizes->budget = $request->budget;
        $centralizes->status_id = $request->status_id;
        $centralizes->creative_type_id = $request->creative_type_id;
        $centralizes->creatives_id = $request->creatives_id;
        $centralizes->date_ad_created = $request->date_ad_created;
        $centralizes->date_ads_run = $request->date_ads_run;
        $centralizes->date_creative_created = $request->date_creative_created;
        $centralizes->ad_created_by = $request->ad_created_by;
        $centralizes->sequence_created_by = $request->sequence_created_by;
        $centralizes->date_sequence_created = $request->date_sequence_created;
        $centralizes->json_created_by = $request->json_created_by;
        $centralizes->date_json_created =$request->date_json_created;
        $centralizes->notes = $request->notes;
        $centralizes->save();

        Session::flash('success','Product was created succesfully!');
        return redirect('/centralized');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $centralizes = Centralize::find($id);
        return view('centralized.show', compact('centralizes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $centralizes = Centralize::find($id);
        $teams = AgentTeam::all();
        $stores = Store::all();
        $campaigns = Campaign::all();
        $strategies = Strategy::all();
        $statuses = Status::all();
        $pages = Page::all();
        $creative_types = CreativeType::all();
        $adsstrategies = AdStrategy::all();
        $adstypes = AdType::all();
        $creatives = User::where('department_id','=','2')->get();
        $adsteam = User::where('department_id','=','3')->get();
        return view('centralized.edit', compact('stores','campaigns','strategies','statuses', 'pages','creative_types','adsstrategies','adstypes','creatives','centralizes','adsteam','teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'store_id' => 'required',
        ]);

        $centralizes = Centralize::find($id);
        $centralizes->product_name = $request->product_name;
        $centralizes->store_id = $request->store_id;
        $centralizes->page_id = $request->page_id;
        $centralizes->post_id = $request->post_id;
        $centralizes->ads_name = $request->ads_name;
        $centralizes->campaign_id = $request->campaign_id;
        $centralizes->strategy_id = $request->strategy_id;
        $centralizes->ads_strategy_id = $request->ads_strategy_id;
        $centralizes->ads_type_id = $request->ads_type_id;
        $centralizes->budget = $request->budget;
        $centralizes->status_id = $request->status_id;
        $centralizes->creative_type_id = $request->creative_type_id;
        $centralizes->creatives_id = $request->creatives_id;
        $centralizes->date_ad_created = $request->date_ad_created;
        $centralizes->date_ads_run = $request->date_ads_run;
        $centralizes->date_creative_created = $request->date_creative_created;
        $centralizes->ad_created_by = $request->ad_created_by;
        $centralizes->sequence_created_by = $request->sequence_created_by;
        $centralizes->date_sequence_created = $request->date_sequence_created;
        $centralizes->json_created_by = $request->json_created_by;
        $centralizes->date_json_created =$request->date_json_created;
        $centralizes->notes = $request->notes;
        $centralizes->date_ads_killed = $request->date_ads_killed;
        $centralizes->save();


        Session::flash('success','Product was created succesfully!');
        return redirect()->route('centralized.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function running()
    {
        $running = Centralize::where('status_id','=',1)->get();
        return view('centralized.running', compact('running'));
    }

    public function killed()
    {
        $killed = Centralize::where('status_id','=',5)->get();
        return view('centralized.killed', compact('killed'));
    }
}

