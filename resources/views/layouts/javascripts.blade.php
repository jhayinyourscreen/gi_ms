<!-- jQuery -->
<script src="{{ asset('../js/backend/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('../js/backend/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('../js/backend/bootstrap.bundle.min.js') }}"></script>

<!-- ChartJS -->
<script src="{{ asset('../js/backend/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('../js/backend/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('../js/backend/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('../js/backend/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('../js/backend/jquery.knob.min.js') }}"></script>

<!-- daterangepicker -->
<script src="{{ asset('../js/backend/moment.min.js') }}"></script>
<script src="{{ asset('../js/backend/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('../js/backend/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('../js/backend/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('../js/backend/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('../js/backend/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('../js/backend/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('../js/backend/dashboard.js') }}"></script>

@yield('javascripts')