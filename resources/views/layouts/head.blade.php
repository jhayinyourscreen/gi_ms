<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Marketing System</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('../css/backend/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <!-- <link rel="stylesheet" href="{{ asset('../css/backend/tempusdominus-bootstrap-4.min.css') }}"> -->
    <!-- iCheck -->
    <!-- <link rel="stylesheet" href="{{ asset('../css/backend/icheck-bootstrap.min.css') }}"> -->
    <!-- JQVMap -->
    <!-- <link rel="stylesheet" href="{{ asset('../css/backend/jqvmap.min.css') }}"> -->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/backend/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('css/backend/OverlayScrollbars.min.css') }}">
    <!-- Daterange picker -->
    <!-- <link rel="stylesheet" href="{{ asset('css/backend/daterangepicker.css') }}"> -->
    <!-- summernote -->
    <!-- <link rel="stylesheet" href="{{ asset('css/backend/summernote-bs4.min.css') }}"> -->

    @yield('stylesheets')

    </head>