<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">GI - MS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
            <a href="{{ Route('dashboard') }}" class="nav-link {{Route::is('dashboard') || Route::is('dashboard/*') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
            </li>
            <li class="nav-item @if(Request::is('store') || Request::is('store/*') || 
            Request::is('departments') || Request::is('departments/*') || 
            Request::is('pages') || Request::is('pages/*') ||
            Request::is('agent-teams') || Request::is('agent-teams/*') ||
            Request::is('employees') || Request::is('employees/*') ||
            Request::is('campaigns') || Request::is('campaigns/*') ||
            Request::is('creative-types') || Request::is('creative-types/*') ||
            Request::is('strategies') || Request::is('strategies/*') ||
            Request::is('status') || Request::is('status/*') ||
            Request::is('ads-type') || Request::is('ads-type/*') ||
            Request::is('ads-strategy') || Request::is('ads-strategy/*')) 
            menu-open @endif">
              <a href="#" class="nav-link @if(Request::is('store') || Request::is('store/*') || 
              Request::is('departments') || Request::is('departments/*') || 
              Request::is('employees') || Request::is('employees') ||
              Request::is('pages') || Request::is('pages/*') ||
              Request::is('agent-teams') || Request::is('agent-teams/*') ||
              Request::is('campaigns') || Request::is('campaigns/*') ||
              Request::is('creative-types') || Request::is('creative-types/*') ||
              Request::is('strategies') || Request::is('strategies/*') ||
              Request::is('status') || Request::is('status/*') ||
              Request::is('ads-type') || Request::is('ads-type/*') ||
              Request::is('ads-strategy') || Request::is('ads-strategy/*')) 
              active @endif">
                <i class="nav-icon fas fa-plus"></i>
                <p>
                  Add
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ URL('employees') }}" class="nav-link @if(Request::is('employees') || Request::is('employees/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Employees</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('agent-teams') }}" class="nav-link @if(Request::is('agent-teams') || Request::is('agent-teams/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Team</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('departments') }}" class="nav-link @if(Request::is('departments') || Request::is('departments/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Departments</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('store') }}" class="nav-link @if(Request::is('store') || Request::is('store/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Stores</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('pages') }}" class="nav-link @if(Request::is('pages') || Request::is('pages/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Pages</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('campaigns') }}" class="nav-link @if(Request::is('campaigns') || Request::is('campaigns/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Campaigns</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('creative-types') }}" class="nav-link @if(Request::is('creative-types') || Request::is('creative-types/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Creative Types</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('strategies') }}" class="nav-link @if(Request::is('strategies') || Request::is('strategies/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Strategies</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('status') }}" class="nav-link @if(Request::is('status') || Request::is('status/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Status</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('ads-type') }}" class="nav-link @if (Request::is('ads-type') || Request::is('ads-type/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ads Type</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('ads-strategy') }}" class="nav-link @if (Request::is('ads-strategy') || Request::is('ads-strategy/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ads Strategy</p>
                  </a>
                </li>
              </ul>
            </li>
            <!-- Start Centralized -->
            <li class="nav-item @if(Request::is('centralized') || Request::is('centralized/*') ||
            Request::is('running') || Request::is('running/*') ||
            Request::is('killed') || Request::is('killed/*'))
            menu-open @endif">
              <a href="#" class="nav-link @if(Request::is('centralized') || Request::is('centralized/*') ||
              Request::is('running') || Request::is('running/*') ||
              Request::is('killed') || Request::is('killed/*'))
              active @endif">
                <i class="nav-icon fas fa-file"></i>
                <p>
                  Centralized
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ URL('centralized') }}" class="nav-link @if(Request::is('centralized') || Request::is('centralized/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>All</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('running') }}" class="nav-link @if(Request::is('running') || Request::is('running/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Running</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Pending</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Paused</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Rejected</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Killed</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item @if(Request::is('budget-per-product') || Request::is('budget-per-product/*')) menu-open @endif">
              <a href="#" class="nav-link @if(Request::is('budget-per-product') || Request::is('budget-per-product/*')) active @endif">
                <i class="nav-icon fas fa-map-pin"></i>
                <p>
                  Trackers
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>ROAS Tracker</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Stores Running</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ URL('budget-per-product') }}" class="nav-link @if(Request::is('budget-per-product') || Request::is('budget-per-product/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Budget per Products</p>
                  </a>
                </li>
              </ul>
            </li>
            <!-- End Centralized -->
            <li class="nav-item">
              <a class="nav-link" href="{{ Route('logout') }}" 
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                  <i class="nav-icon fas fa-sign-out-alt"></i>
                  <p>
                    Logout
                  </p>
                </a>
                <form id="logout-form" action="{{ Route('logout') }}" method="POST">
                @csrf
                </form>
              </a>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
