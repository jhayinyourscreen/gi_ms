@extends('main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/backend/custom.css') }}">
@endsection


@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Budget per Store</h1>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
            <div class="card">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Store</th>
                      <th>Total Campaign</th>
                      <th>MSG Total Campaign</th>
                      <th>Budget</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($showstores as $index => $showstore)
                    <tr>
                      <td>{{$index +1}}</td>
                      <td style="text-transform: uppercase;">
                      <span class="@if($showstore->stores->team_id == 1) badge team-yzcen
                      @elseif($showstore->stores->team_id == 2) badge team-cams
                      @elseif($showstore->stores->team_id == 3) badge team-fely
                      @endif" style="font-size: 15px;">{{$showstore->stores->store_code}}
                      </span>
                      </td>
                      <td>{{$showstore->total_products}}</td>
                      <td>
                      </td>
                      <td>{{$showstore->budget}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Budget per Products</h1>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
            <div class="card">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Products</th>
                      <th>Store</th>
                      <th>Budget</th>
                      <th>Campaign</th>
                      <th>Date Run</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($showproducts as $showproduct)
                    <tr>
                      <td>{{$showproduct->id}}</td>
                      <td style="text-transform: capitalize;">{{$showproduct->product_name}}</td>
                      <td style="text-transform: uppercase;">
                      <span class="@if($showproduct->stores->team_id == 1) badge team-yzcen
                      @elseif($showproduct->stores->team_id == 2) badge team-cams
                      @elseif($showproduct->stores->team_id == 3) badge team-fely
                      @endif" style="font-size: 15px;">{{$showproduct->stores->store_code}}
                      </span>
                      </td>
                      <td>{{$showproduct->budget}}</td>
                      <td>
                      <span class="@if($showproduct->campaign_id == 1) badge bg-success
                      @elseif($showproduct->campaign_id == 2) badge bg-warning
                      @endif">
                      @if(!empty($showproduct->campaign_id)) {{$showproduct->campaigns->campaign_code}} @endif
                      </span>
                      </td>
                      <td>
                        @if(!empty($showproduct->date_ads_run)) {{$showproduct->date_ads_run->format('F d, Y')}} @endif
                      </td>
                      <td>
                        <a href="{{ URL('centralized',$showproduct->id) }}">
                        <button type="button" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></button>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
              <div class="d-flex justify-content-center">
                {{$showproducts->links('pagination::bootstrap-4')}}
              </div>
            <!-- /.card -->
          </div>
          

@endsection

@section('javascripts') 

<!-- DataTables  & Plugins -->
<script src="{{ asset('../js/backend/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('../js/backend/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/jszip.min.js') }}"></script>
<script src="{{ asset('../js/backend/pdfmake.min.js') }}"></script>
<script src="{{ asset('../js/backend/vfs_fonts.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.print.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection