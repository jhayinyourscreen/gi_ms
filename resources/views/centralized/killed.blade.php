@extends('main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/backend/custom.css') }}">
@endsection


@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Killed</h1>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <div class="card">
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Ref #</th>
                    <th>Products</th>
                    <th>Store</th>
                    <th>Pages</th>
                    <th>Date Run</th>
                    <th>Date Killed</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($killed as $kill)
                  <tr>
                    <td>{{$kill->id}}</td>
                    <td style="text-transform: capitalize;">{{$kill->product_name}}</td>
                    <td style="text-transform: uppercase;"
                    class="@if($kill->stores->team_id == 1) team-yzcen
                    @elseif($kill->stores->team_id == 2) team-cams
                    @elseif($kill->stores->team_id == 3) team-fely
                    @endif">
                    {{$kill->stores->store_code}}
                    </td>
                    <td style="text-transform: capitalize;">@if(!empty($centralize->page_id)) {{$centralize->pages->page_name}} @endif</td>
                    <td>{{$kill->date_ads_run}}</td>
                    <td></td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button>
                            <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button>
                            <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </div>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascripts')

<!-- DataTables  & Plugins -->
<script src="{{ asset('../js/backend/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('../js/backend/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/jszip.min.js') }}"></script>
<script src="{{ asset('../js/backend/pdfmake.min.js') }}"></script>
<script src="{{ asset('../js/backend/vfs_fonts.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.print.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection