@extends('main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/backend/custom.css') }}">
@endsection

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0" style="text-transform: capitalize;">#{{$centralizes->id}} - {{$centralizes->product_name}}({{$centralizes->stores->store_name}})
                @if(!empty($centralizes->strategy_id))* {{$centralizes->strategies->strategy_name}} @endif
                @if(!empty($centralizes->campaign_id))* {{$centralizes->campaigns->campaign_code}} @endif
                @if(!empty($centralizes->creative_type_id))* {{$centralizes->creative_types->creative_types}} @endif
                @if(!empty($centralizes->date_ads_run))* {{$centralizes->date_ads_run->format('F d, Y')}} @endif</h1>
            </div>
        </div>
    </div>
</div>

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ URL('centralized') }}" class="btn btn-md btn-secondary">Back</a>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <a href="{{ URL('centralized/'.$centralizes->id.'/edit') }}" class="btn btn-md btn-success">Edit</a>
            </ol>
          </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="{{$centralizes->product_name}}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label>Store</label>
                                    <input class="form-control" style="text-transform: uppercase;" value="{{$centralizes->stores->store_code}}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Page Name</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->page_id)) {{$centralizes->pages->page_name}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Strategy</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->strategy_id)) {{$centralizes->strategies->strategy_name}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Campaign</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->campaign_id)) {{$centralizes->campaigns->campaign_name}}({{$centralizes->campaigns->campaign_code}}) @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Type</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->ads_type_id)) {{$centralizes->adstypes->ads_type}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Strategies</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->ads_strategy_id)) {{$centralizes->adsstrategies->ads_strategy}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->status_id)) {{$centralizes->statuses->status}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Creative Types</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->creative_type_id)) {{$centralizes->creative_types->creative_types}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Budget</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->budget)) {{$centralizes->budget}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Ads Name</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->ads_name)) {{$centralizes->ads_name}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label>Post ID</label>
                                    <input class="form-control" value="@if(!empty($centralizes->post_id)) {{$centralizes->post_id}} @endif" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Creatives Created By</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->creatives_id)) {{$centralizes->creatives->f_name}} {{$centralizes->creatives->l_name}}@endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Created By</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->ad_created_by)) {{$centralizes->adsteam->f_name}} {{$centralizes->adsteam->l_name}}@endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>JSON Created By</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->json_created_by)) {{$centralizes->json->f_name}} {{$centralizes->json->l_name}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Sequence Created By</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->sequence_created_by)) {{$centralizes->sequences->f_name}} {{$centralizes->sequences->l_name}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Creatives Created</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_creative_created)) {{$centralizes->date_creative_created->format('F d, Y')}}@endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Ads Created</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ad_created)) {{$centralizes->date_ad_created->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date JSON Created</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_json_created)) {{$centralizes->date_json_created->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Sequence Created</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_sequence_created)) {{$centralizes->date_sequence_created->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Assign</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->created_at)) {{$centralizes->created_at->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Run</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_run)) {{$centralizes->date_ads_run->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            @if($centralizes->status_id == 5)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Killed</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_killed)) {{$centralizes->date_ads_killed->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            @endif
                            @if($centralizes->status_id == 2)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Paused</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_paused)) {{$centralizes->date_ads_paused->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            @endif
                            @if($centralizes->status_id == 3)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Rejected</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_run)) {{$centralizes->date_ads_run->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            @endif
                            @if($centralizes->status_id == 4)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Disabled</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_run)) {{$centralizes->date_ads_run->format('F d, Y')}} @endif" disabled>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea class="form-control" rows="3" disabled>{{$centralizes->notes}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection