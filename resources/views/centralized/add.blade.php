<!-- Start Modal -->
<div class="modal fade" id="add-product">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add Product</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="{{route('centralized.store')}}">
                            @csrf
                                <div class="form-group">
                                    <label>Product</label>
                                    <input style="text-transform: capitalize;" type="text" class="form-control" name="product_name" placeholder="Product Name">
                                </div>
                                <div class="form-group">
                                    <label>Store</label>
                                    <select class="form-control select2" style="width: 100%; text-transform: capitalize;" name="store_id">
                                        <option value=""selected>-- Select Store --</option>
                                        @foreach($stores as $store)
                                        <option value="{{$store->id}}">{{$store->store_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Page</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="page_id">
                                        <option value="" selected>-- Select Page --</option>
                                        @foreach($pages as $page)
                                        <option value="{{$page->id}}">{{$page->page_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Post ID</label>
                                    <input type="text" class="form-control" name="post_id" placeholder="Post ID">
                                </div>
                                <div class="form-group">
                                    <label>Ads Name</label>
                                    <input style="text-transform: capitalize;" type="text" class="form-control" name="ads_name" placeholder="Ads Name">
                                </div>
                                <div class="form-group">
                                    <label>Campaign</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="campaign_id">
                                        <option value=""selected>-- Select Campaign --</option>
                                        @foreach($campaigns as $campaign)
                                        <option value="{{$campaign->id}}">{{$campaign->campaign_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Product Type</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="strategy_id">
                                        <option value=""selected>-- Select Product Type --</option>
                                        @foreach($strategies as $strategy)
                                        <option value="{{$strategy->id}}">{{$strategy->strategy_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Ads Strategy</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ads_strategy_id">
                                        <option value=""selected>-- Select Ads Strategy --</option>
                                        @foreach($adsstrategies as $adsstrategy)
                                        <option value="{{$adsstrategy->id}}">{{$adsstrategy->ads_strategy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Ads Type</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ads_type_id">
                                        <option value=""selected>-- Select Ads Type --</option>
                                        @foreach($adstypes as $adstype)
                                        <option value="{{$adstype->id}}">{{$adstype->ads_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Budget</label>
                                    <input style="text-transform: capitalize;" type="number" class="form-control" placeholder="Enter Amount" name="budget">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select style="text-transform: capitalize;"class="form-control select2" style="width: 100%;" name="status_id">
                                        <option value=""selected>-- Select Status --</option>
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Creative Type</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="creative_type_id">
                                        <option value=""selected>-- Select Creative Type --</option>
                                        @foreach($creative_types as $creative_type)
                                        <option value="{{$creative_type->id}}">{{$creative_type->creative_types}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Creative Creator</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="creatives_id">
                                        <option value="" selected>-- Select Creative Creator --</option>
                                        @foreach($creatives as $creative)
                                        <option value="{{$creative->id}}">{{$creative->f_name}} {{$creative->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- Date mm/dd/yyyy -->
                                <div class="form-group">
                                    <label>Date Creatives Created</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="text" name="date_creative_created" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Ads Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ad_created_by">
                                        <option value="" selected>-- Select Ad Created By --</option>
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}">{{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- Date mm/dd/yyyy -->
                                <div class="form-group">
                                    <label>Date Ad Created</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="text" name="date_ad_created" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- Date mm/dd/yyyy -->
                                <div class="form-group">
                                    <label>Date Ads Run</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="text" name="date_ads_run" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->