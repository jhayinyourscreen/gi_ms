@extends('main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/backend/custom.css') }}">
@endsection

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0" style="text-transform: capitalize;">#{{$centralizes->id}} - {{$centralizes->product_name}}({{$centralizes->stores->store_name}})
                @if(!empty($centralizes->strategy_id))* {{$centralizes->strategies->strategy_name}} @endif
                @if(!empty($centralizes->campaign_id))* {{$centralizes->campaigns->campaign_code}} @endif
                @if(!empty($centralizes->creative_type_id))* {{$centralizes->creative_types->creative_types}} @endif
                @if(!empty($centralizes->date_ads_run))* {{$centralizes->date_ads_run->format('F d, Y')}} @endif</h1>
            </div>
        </div>
    </div>
</div>

<form method="POST" action="{{ route('centralized.update', $centralizes->id) }}">
@method('PUT')
@csrf
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ URL('centralized') }}" class="btn btn-md btn-secondary">Back</a>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <button type="submit" class="btn btn-primary">Update</button>
            </ol>
          </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" style="text-transform: capitalize;" name="product_name" value="{{$centralizes->product_name}}">
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label>Store</label>
                                    <select class="form-control select2" style="width: 100%; text-transform: uppercase;" name="store_id">
                                        @foreach($stores as $store)
                                        <option value="{{$store->id}}" {{ ($store->id == $centralizes->store_id) ? 'selected' : ''}}>{{$store->store_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Page Name</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="page_id">
                                        @if(empty($centralizes->page_id))
                                        <option value=""> --Select Page-- </option>
                                        @endif
                                        @foreach($pages as $page)
                                        <option value="{{$page->id}}" @if(!empty($centralizes->page_id == $page->id)) selected @endif>{{$page->page_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Strategy</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="strategy_id">
                                        @if(empty($centralizes->strategy_id))
                                        <option value="">-- Select Strategy --</option>
                                        @endif
                                        @foreach($strategies as $strategy)
                                        <option value="{{$strategy->id}}" @if(!empty($centralizes->strategy_id == $strategy->id)) selected @endif>{{$strategy->strategy_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Campaign</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="campaign_id">
                                        @if(empty($centralizes->campaign_id))
                                        <option value="">-- Select Strategy --</option>
                                        @endif
                                        @foreach($campaigns as $campaign)
                                        <option value="{{$campaign->id}}" @if(!empty($centralizes->campaign_id == $campaign->id)) selected @endif>{{$campaign->campaign_name}}({{$campaign->campaign_code}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Type</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ads_type_id">
                                        @if(empty($centralizes->ads_type_id))
                                        <option value="">-- Select Ads Type --</option>
                                        @endif
                                        @foreach($adstypes as $adstype)
                                        <option value="{{$adstype->id}}" @if(!empty($centralizes->ads_type_id == $adstype->id)) selected @endif>{{$adstype->ads_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Strategies</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ads_strategy_id">
                                        @if(empty($centralizes->ads_strategy_id))
                                        <option value="">-- Select Strategy --</option>
                                        @endif
                                        @foreach($adsstrategies as $adsstrategy)
                                        <option value="{{$adsstrategy->id}}" @if(!empty($centralizes->ads_strategy_id == $adsstrategy->id)) selected @endif>{{$adsstrategy->ads_strategy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="status_id">
                                        @if(empty($centralizes->status_id))
                                        <option value="">-- Select Status --</option>
                                        @endif
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}" @if(!empty($centralizes->status_id == $status->id)) selected @endif>{{$status->status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Creative Types</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="creative_type_id">
                                        @if(empty($centralizes->creative_type_id))
                                        <option value="">-- Select Creative Type --</option>
                                        @endif
                                        @foreach($creative_types as $creativetype)
                                        <option value="{{$creativetype->id}}" @if(!empty($centralizes->creative_type_id == $creativetype->id)) selected @endif>{{$creativetype->creative_types}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Budget</label>
                                    <input type="number" class="form-control" name="budget" value="@if(!empty($centralizes->budget)){{$centralizes->budget}}@endif">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Ads Name</label>
                                    <input class="form-control" style="text-transform: capitalize;" name="ads_name" value="@if(!empty($centralizes->ads_name)) {{$centralizes->ads_name}} @endif">
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label>Post ID</label>
                                    <input class="form-control" name="post_id" value="@if(!empty($centralizes->post_id)) {{$centralizes->post_id}}@endif">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Creatives Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="creatives_id">
                                        @if(empty($centralizes->creatives_id))
                                        <option value="">-- Select Creative Creator --</option>
                                        @endif
                                        @foreach($creatives as $creative)
                                        <option value="{{$creative->id}}" @if(!empty($centralizes->creatives_id == $creative->id)) selected @endif>
                                        {{$creative->f_name}} {{$creative->l_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ad_created_by">
                                        @if(empty($centralizes->ad_created_by))
                                        <option value="">-- Select Ads Creator --</option>
                                        @endif
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}" @if(!empty($centralizes->ad_created_by == $ads->id)) selected @endif>
                                        {{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>JSON Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="json_created_by">
                                        @if(empty($centralizes->json_created_by))
                                        <option value="" selected>-- Select JSON Creator --</option>
                                        @endif
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}" @if(!empty($centralizes->json_created_by == $ads->id)) selected @endif>
                                        {{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Sequence Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="sequence_created_by">
                                        @if(empty($centralizes->sequence_created_by))
                                        <option value="" selected>-- Select Sequence Creator --</option>
                                        @endif
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}" @if(!empty($centralizes->sequence_created_by == $ads->id)) selected @endif>
                                        {{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Creatives Created</label>
                                    <div class="input-group date" id="datecreativescreated" data-target-input="nearest">
                                        <input type="text" name="date_creative_created" class="form-control datetimepicker-input" data-target="#datecreativescreated"
                                        value="@if(!empty($centralizes->date_creative_created)) {{$centralizes->date_creative_created->format('m/d/Y')}}@endif">
                                        <div class="input-group-append" data-target="#datecreativescreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Ads Created</label>
                                    <div class="input-group date" id="dateadscreated" data-target-input="nearest">
                                        <input type="text" name="date_ad_created" class="form-control datetimepicker-input" data-target="#dateadscreated"
                                        value="@if(!empty($centralizes->date_ad_created)) {{$centralizes->date_ad_created->format('m/d/Y')}}@endif">
                                        <div class="input-group-append" data-target="#dateadscreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date JSON Created</label>
                                    <div class="input-group date" id="datejsoncreated" data-target-input="nearest">
                                        <input type="text" name="date_json_created" class="form-control datetimepicker-input" data-target="#datejsoncreated"
                                        value="@if(!empty($centralizes->date_json_created)) {{$centralizes->date_json_created->format('m/d/Y')}}@endif">
                                        <div class="input-group-append" data-target="#datejsoncreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Sequence Created</label>
                                    <div class="input-group date" id="datesequencecreated" data-target-input="nearest">
                                        <input type="text" name="date_sequence_created" class="form-control datetimepicker-input" data-target="#datesequencecreated"
                                        value="@if(!empty($centralizes->date_sequence_created)) {{$centralizes->date_sequence_created->format('m/d/Y')}}@endif">
                                        <div class="input-group-append" data-target="#datesequencecreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Assign</label>
                                    <div class="input-group date" id="dateadcreated" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#dateadcreated"
                                        value="{{ $centralizes->created_at->format('m/d/Y') }}" disabled>
                                        <div class="input-group-append" data-target="#dateadcreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Run</label>
                                    <div class="input-group date" id="daterun" data-target-input="nearest">
                                        <input type="text" name="date_ads_run" class="form-control datetimepicker-input" data-target="#daterun"
                                        value="@if(!empty($centralizes->date_ads_run)) {{ $centralizes->date_ads_run->format('m/d/Y')}} @endif">
                                        <div class="input-group-append" data-target="#daterun" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($centralizes->status_id == 5)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Killed</label>
                                    <div class="input-group date" id="datekilled" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="date_ads_killed" data-target="#datekilled"
                                        value="@if(!empty($centralizes->date_ads_killed)) {{ $centralizes->date_ads_killed->format('m/d/Y') }} @endif">
                                        <div class="input-group-append" data-target="#datekilled" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($centralizes->status_id == 2)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Paused</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_paused)) {{$centralizes->date_ads_paused->format('F d, Y')}} @endif">
                                </div>
                            </div>
                            @endif
                            @if($centralizes->status_id == 3)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Rejected</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_run)) {{$centralizes->date_ads_run->format('F d, Y')}} @endif">
                                </div>
                            </div>
                            @endif
                            @if($centralizes->status_id == 4)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Disabled</label>
                                    <input class="form-control" style="text-transform: capitalize;" value="@if(!empty($centralizes->date_ads_run)) {{$centralizes->date_ads_run->format('F d, Y')}} @endif">
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Notes</label>
                                    <textarea class="form-control" name="notes" rows="3">@if(!empty($centralizes->notes)){{$centralizes->notes}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('javascripts')

<script>
    $(function() {
        //Date range picker
        $('#datecreativescreated').datetimepicker({
            format: 'L'
        });
        $('#dateadscreated').datetimepicker({
            format: 'L'
        });
        $('#datejsoncreated').datetimepicker({
            format: 'L'
        });
        $('#datesequencecreated').datetimepicker({
            format: 'L'
        });
        $('#dateadcreated').datetimepicker({
            format: 'L'
        });
        $('#daterun').datetimepicker({
            format: 'L'
        });
        $('#datekilled').datetimepicker({
            format: 'L'
        });
        $('#datepaused').datetimepicker({
            format: 'L'
        });
        $('#daterejected').datetimepicker({
            format: 'L'
        });
        $('#datedisabled').datetimepicker({
            format: 'L'
        });
    });
</script>

<script src="{{ asset('../js/backend/dropzone.min.js') }}"></script>

@endsection