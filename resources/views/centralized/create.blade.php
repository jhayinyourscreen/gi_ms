@extends('main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/backend/custom.css') }}">
@endsection

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<form method="POST" action="{{route('centralized.store')}}">
@csrf
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ URL('centralized') }}" class="btn btn-md btn-secondary">Back</a>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <a href="{{ URL('dashboard') }}">
                    <button type="submit" class="btn btn-md btn-primary">Save</button>
                </a>
            </ol>
          </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" name="product_name" style="text-transform: capitalize;" placeholder="Product Name">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Store</label>
                                    <select class="form-control select2" style="width: 100%;" name="store_id">
                                        <option value=""selected>-- Select Store --</option>
                                        @foreach($stores as $store)
                                        <option value="{{$store->id}}" style="text-transform: uppercase;">{{$store->store_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Page</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="page_id">
                                        <option value="" selected>-- Select Page --</option>
                                        @foreach($pages as $page)
                                        <option value="{{$page->id}}">{{$page->page_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Strategy</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="strategy_id">
                                        <option value=""selected>-- Select Strategy --</option>
                                        @foreach($strategies as $strategy)
                                        <option value="{{$strategy->id}}">{{$strategy->strategy_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Campaign</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="campaign_id">
                                        <option value=""selected>-- Select Campaign --</option>
                                        @foreach($campaigns as $campaign)
                                        <option value="{{$campaign->id}}">{{$campaign->campaign_name}}({{$campaign->campaign_code}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Type</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ads_type_id">
                                        <option value=""selected>-- Select Ads Type --</option>
                                        @foreach($adstypes as $adstype)
                                        <option value="{{$adstype->id}}">{{$adstype->ads_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Strategies</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ads_strategy_id">
                                        <option value=""selected>-- Select Ads Strategy --</option>
                                        @foreach($adsstrategies as $adsstrategy)
                                        <option value="{{$adsstrategy->id}}">{{$adsstrategy->ads_strategy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select style="text-transform: capitalize;"class="form-control select2" style="width: 100%;" name="status_id">
                                        <option value=""selected>-- Select Status --</option>
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Creative Types</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="creative_type_id">
                                        <option value=""selected>-- Select Creative Type --</option>
                                        @foreach($creative_types as $creative_type)
                                        <option value="{{$creative_type->id}}">{{$creative_type->creative_types}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Budget</label>
                                    <input type="number" class="form-control" style="text-transform: capitalize;" name="budget" placeholder="0">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Ads Name</label>
                                    <input class="form-control" name="ads_name" placeholder="Ads Name">
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label>Post ID</label>
                                    <input class="form-control" name="post_id" placeholder="Post ID">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Creatives Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="creatives_id">
                                        <option value="" selected>-- Select Creative Creator --</option>
                                        @foreach($creatives as $creative)
                                        <option value="{{$creative->id}}">{{$creative->f_name}} {{$creative->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Ads Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="ad_created_by">
                                        <option value="" selected>-- Select Ads Creator --</option>
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}">{{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>JSON Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="json_created_by">
                                        <option value="" selected>-- Select JSON Creator --</option>
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}">{{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Sequence Created By</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="sequence_created_by">
                                        <option value="" selected>-- Select Sequence Creator --</option>
                                        @foreach($adsteam as $ads)
                                        <option value="{{$ads->id}}">{{$ads->f_name}} {{$ads->l_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Creatives Created</label>
                                    <div class="input-group date" id="datecreativescreated" data-target-input="nearest">
                                        <input type="text" name="date_creative_created" class="form-control datetimepicker-input" data-target="#datecreativescreated"/>
                                        <div class="input-group-append" data-target="#datecreativescreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Ads Created</label>
                                    <div class="input-group date" id="dateadscreated" data-target-input="nearest">
                                        <input type="text" name="date_ad_created" class="form-control datetimepicker-input" data-target="#dateadscreated"/>
                                        <div class="input-group-append" data-target="#dateadscreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date JSON Created</label>
                                    <div class="input-group date" id="datejsoncreated" data-target-input="nearest">
                                        <input type="text" name="date_json_created" class="form-control datetimepicker-input" data-target="#datejsoncreated"/>
                                        <div class="input-group-append" data-target="#datejsoncreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date Sequence Created</label>
                                    <div class="input-group date" id="datesequencecreated" data-target-input="nearest">
                                        <input type="text" name="date_sequence_created" class="form-control datetimepicker-input" data-target="#datesequencecreated"/>
                                        <div class="input-group-append" data-target="#datesequencecreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Assign</label>
                                    <div class="input-group date" id="dateadcreated" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#dateadcreated" 
                                        value="{{ date('m/d/Y') }}" disabled/>
                                        <div class="input-group-append" data-target="#dateadcreated" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Run</label>
                                    <div class="input-group date" id="daterun" data-target-input="nearest">
                                        <input type="text" name="date_ads_run" class="form-control datetimepicker-input" data-target="#daterun"/>
                                        <div class="input-group-append" data-target="#daterun" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Note</label>
                                    <textarea name="notes" class="form-control" rows="3" placeholder="Enter..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('javascripts')

<script>
    $(function() {
        //Date range picker
        $('#datecreativescreated').datetimepicker({
            format: 'L'
        });
        $('#dateadscreated').datetimepicker({
            format: 'L'
        });
        $('#datejsoncreated').datetimepicker({
            format: 'L'
        });
        $('#datesequencecreated').datetimepicker({
            format: 'L'
        });
        $('#dateadcreated').datetimepicker({
            format: 'L'
        });
        $('#daterun').datetimepicker({
            format: 'L'
        });
    });
</script>

<script src="{{ asset('../js/backend/dropzone.min.js') }}"></script>

@endsection