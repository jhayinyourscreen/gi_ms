<!-- Show Modal -->
<div class="modal fade" id="show-product-{{$centralize->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 style="text-transform: capitalize;" class="modal-title">{{$centralize->product_name}}</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Store</label>
                                                <input type="text" class="form-control" value="{{$centralize->stores->store_code}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Page</label>
                                                <input type="text" class="form-control" value="@if(!empty($centralize->page_id)) {{$centralize->pages->page_name}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->status_id)) {{$centralize->statuses->status}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Post ID</label> <a href="https://{{$centralize->post_id}}">Link</a>
                                                <input type="text" class="form-control" value="@if(!empty($centralize->post_id)) {{$centralize->post_id}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Ads Name</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->ads_name)) {{$centralize->ads_name}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Campaign</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->campaign_id)) {{$centralize->campaigns->campaign_name}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Product Type</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->strategy_id)) {{$centralize->strategies->strategy_name}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Ads Strategy</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->ads_strategy_id)) {{$centralize->adsstrategies->ads_strategy}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Ads Type</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->ads_type_id)) {{$centralize->adstypes->ads_type}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Budget</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->budget)) {{$centralize->budget}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Creative Type</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->creative_type_id)) {{$centralize->creative_types->creative_types}} @endif" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Creative Creator</label>
                                                <input style="text-transform: capitalize;" type="text" class="form-control" value="@if(!empty($centralize->creatives_id)) {{$centralize->creatives->f_name}} {{$centralize->creatives->l_name}} @endif" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Show Modal -->