@extends('main')

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/backend/custom.css') }}">
@endsection

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Centralized</h1>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a href="{{ URL('centralized/create') }}">
                <button type="button" class="btn btn-md btn-primary">Add<i class="fas fa-plus"></i></button>
                </a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Reference #</th>
                    <th>Products</th>
                    <th>Store</th>
                    <th>Pages</th>
                    <th>Budget</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($centralizes as $centralize)
                  <tr>
                    <td>{{$centralize->id}}</td>
                    <td style="text-transform: capitalize;">{{ Str::limit($centralize->product_name, 15) }}</td>
                    <td style="text-transform: uppercase;"
                    class="@if($centralize->stores->team_id == 1) team-yzcen
                    @elseif($centralize->stores->team_id == 2) team-cams
                    @elseif($centralize->stores->team_id == 3) team-fely
                    @endif">
                    @if(!empty($centralize->store_id)) {{$centralize->stores->store_code}} @endif
                    </td>
                    <td style="text-transform: capitalize;">
                    @if(!empty($centralize->page_id)) {{$centralize->pages->page_name}} @endif
                    </td>
                    <td>@if(!empty($centralize->budget)) {{$centralize->budget}} @endif</td>
                    <td style="text-transform: capitalize;"
                    class="
                    @if($centralize->status_id == 1) status-running
                    @elseif($centralize->status_id == 2) status-paused
                    @elseif($centralize->status_id == 3) status-rejected
                    @elseif($centralize->status_id == 4) status-disabled  
                    @elseif($centralize->status_id == 5) status-killed @endif">
                    @if(!empty($centralize->status_id)) {{$centralize->statuses->status}}@endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ URL('centralized',$centralize->id)}}">
                            <button type="button" class="btn btn-secondary"><i class="fas fa-eye"></i></button>
                            </a>
                            <a href="{{ URL('centralized/'.$centralize->id.'/edit') }}">
                            <button type="button" class="btn btn-success"><i class="fas fa-edit"></i></button>
                            </a>
                            <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </div>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascripts')

<!-- DataTables  & Plugins -->
<script src="{{ asset('../js/backend/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('../js/backend/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('../js/backend/jszip.min.js') }}"></script>
<script src="{{ asset('../js/backend/pdfmake.min.js') }}"></script>
<script src="{{ asset('../js/backend/vfs_fonts.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.print.min.js') }}"></script>
<script src="{{ asset('../js/backend/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('../js/backend/jquery.inputmask.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    })
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    $('[data-mask]').inputmask()
  });
</script>

@endsection