@extends('main')

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Pages</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#add-pagenames">Add <i class=" fas fa-plus"></i></button>
                <!-- User Create Modal -->
                <div class="modal fade" id="add-pagenames">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title">Add Pages</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="{{ route('pages.store') }}">
                            @csrf
                                <div class="form-group">
                                    <label>Store Assign</label>
                                    <select class="form-control select2" style="width: 100%;" name="store_id">
                                      <option selected>-- Select Store to Assign --</option>
                                      @foreach($stores as $store)
                                      <option value="{{$store->id}}">{{$store->store_name}}</option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Page Name</label>
                                    <input type="text" class="form-control" name="page_name" placeholder="Page Name">
                                </div>
                                <div class="form-group">
                                    <label>Page Link</label>
                                    <input type="text" class="form-control" name="links" placeholder="Page URL">
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>Page Name</th>
                      <th>Store Assign</th>
                      <th>Link</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($pages as $page)
                    <tr>
                      <td>{{$page->page_name}}</td>
                      <td>{{$page->stores->store_name}}</td>
                      <td><a href="https://{{$page->links}}">Link</a></td>
                      <td>
                        <div class="btn-group">
                            <a href="#"><button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button></a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit-page-{{$page->id}}"><i class="fas fa-edit"></i></button>
                            <!-- Page Edit Modal -->
                            <div class="modal fade" id="edit-page-{{$page->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Edit Page</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        <form method="POST" action="{{ route('pages.update',$page->id) }}">
                                        @method('PUT')
                                        @csrf
                                            <div class="form-group">
                                                <label>Page Name</label>
                                                <input type="text" class="form-control" name="page_name" value="{{$page->page_name}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Store Assign</label>
                                                <select class="form-control select2" style="width: 100%;" name="store_id">
                                                <option value="{{$page->store_id}}">{{$page->stores->store_name}}</option>
                                                @foreach($stores as $store)
                                                <option value="{{$store->id}}">{{$store->store_name}}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Page Link</label>
                                                <input type="text" class="form-control" name="links" value="{{$page->links}}">
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Update</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-page-{{$page->id}}"><i class="fas fa-trash"></i></button>
                            <!-- Delete Page Modal -->
                            <div class="modal fade" id="delete-page-{{$page->id}}">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title">Are you sure to delete this page?</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body" style="text-align: right;">
                                  <form method="POST" action="{{ route('pages.destroy',$page->id) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-danger">Delete</button>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal -->
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
    </div>
</section>

<script>
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
</script>

@endsection