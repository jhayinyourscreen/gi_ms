@extends('main')

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Employees</h1>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#add-employees">Add<i class="fas fa-plus"></i></button>
                        <!-- Add Departments Modal -->
                        <div class="modal fade" id="add-employees">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title">Add Employee</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    <form method="POST" action="{{ route('employees.store') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input style="text-transform: capitalize;" type="text" class="form-control" name="f_name" placeholder="First Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input style="text-transform: capitalize;" type="text" class="form-control" name="l_name" placeholder="Last Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label>Department</label>
                                            <select class="form-control select2" style="width: 100%;" name="department_id">
                                            <option selected>-- Select Department --</option>
                                            @foreach($departments as $department)
                                            <option value="{{$department->id}}">{{$department->department_name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>is Team Leader?</label>
                                            <select class="form-control select2" style="width: 100%;" name="is_team_leader">
                                            <option selected>-- Is Team Leader? --</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modal -->
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>Employee Name</th>
                                    <th>Department</th>
                                    <th>Position</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $employee)
                                <tr>
                                    <td style="text-transform: capitalize;">{{ $employee->f_name}} {{$employee->l_name}}</td>
                                    <td style="text-transform: capitalize;">@if(!empty($employee->department_id)) {{ $employee->departments->department_name}} @endif</td>
                                    <td style="text-transform: capitalize;">@if($employee->is_team_leader == 1) Team Leader @else Member @endif</td>
                                    <td>{{$employee->email}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection