@extends('main')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{$stores->store_name}}</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#add-pagenames">Add <i class=" fas fa-plus"></i></button>
                <!-- User Create Modal -->
                <div class="modal fade" id="add-pagenames">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title">Add Page Name</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="#">
                            @csrf
                                <div class="form-group">
                                    <label>Store Name</label>
                                    <input type="text" class="form-control" name="page_name" placeholder="Page Name">
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>Store Name</th>
                      <th>Store Code</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Example</td>
                      <td>Example</td>
                      <td>
                        <div class="btn-group">
                            <a href="#"><button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button></a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#"><i class="fas fa-edit"></i></button>
                            <!-- User Create Modal -->
                            <div class="modal fade" id="#">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Edit Page Name</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        <form method="#" action="#">
                                            <div class="form-group">
                                                <label>Store Name</label>
                                                <input type="text" class="form-control" name="" value="">
                                            </div>
                                            <div class="form-group">
                                                <label>Store Code</label>
                                                <input type="text" class="form-control" name="" value="">
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Update</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
    </div>
</section>

@endsection