@extends('main')

@section('stylesheets')

<link rel="stylesheet" href="{{ asset('../css/backend/bootstrap-4.min.css') }}">

@endsection

@section('content')

@if(session('success'))
<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h5><i class="icon fas fa-check"></i> Success!</h5>
{{session('success')}}
</div>
@elseif($errors->any())
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fas fa-times"></i>Error!</h5>
@foreach($errors->all() as $error)
  <p>{{$error}}</p>
@endforeach
</div>
@endif

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Stores</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
    <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#add-stores">Add <i class=" fas fa-plus"></i></button>
                <!-- User Create Modal -->
                <div class="modal fade" id="add-stores">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title">Add Store</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="{{ route('store.store') }}">
                            @csrf
                                <div class="form-group">
                                    <label>Store Name</label>
                                    <input type="text" style="text-transform: capitalize;" class="form-control" name="store_name" placeholder="Store Name">
                                </div>
                                <div class="form-group">
                                    <label>Store Code</label>
                                    <input type="text" style="text-transform: uppercase;"class="form-control" name="store_code" placeholder="Store Code">
                                </div>
                                <div class="form-group">
                                    <label>Team Assign</label>
                                    <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="team_id">
                                        <option value=""selected>-- Select Team --</option>
                                        @foreach($teams as $team)
                                        <option value="{{$team->id}}">{{$team->agent_team}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>Store Name</th>
                      <th>Store Code</th>
                      <th>Team</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($stores as $store)
                    <tr>
                      <td style="text-transform: capitalize;">{{ $store->store_name }}</td>
                      <td style="text-transform: uppercase;">{{ $store->store_code }}</td>
                      <td style="text-transform: capitalize;">@if(!empty($store->team_id)){{ $store->teams->agent_team}} @endif</td>
                      <td>
                        <div class="btn-group">
                            <!-- <a href="{{ route('store.show',$store->id) }}"><button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button></a> -->
                            <a href="#"><button type="button" class="btn btn-success"><i class="fas fa-eye"></i></button></a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit-stores-{{$store->id}}"><i class="fas fa-edit"></i></button>
                            <!-- User Create Modal -->
                            <div class="modal fade" id="edit-stores-{{$store->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Edit Store</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        <form method="POST" action="{{ route('store.update', $store->id) }}">
                                        @method('PUT')
                                        @csrf
                                            <div class="form-group">
                                                <label>Store Name</label>
                                                <input type="text" class="form-control" name="store_name" value="{{ $store->store_name }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Store Code</label>
                                                <input type="text" class="form-control" name="store_code" value="{{ $store->store_code }}">
                                            </div>
                                            <div class="form-group">
                                            <label>Team Assign</label>
                                              <select style="text-transform: capitalize;" class="form-control select2" style="width: 100%;" name="team_id">
                                                <option value="@if(!empty($store->team_id)) {{$store->team_id}} @endif" selected>-- Select Team --</option>
                                                @foreach($teams as $team)
                                                <option value="{{$team->id}}">{{$team->agent_team}}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Update</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-store-{{$store->id}}"><i class="fas fa-trash"></i></button>
                            <!-- Delete Store Modal -->
                            <div class="modal fade" id="delete-store-{{$store->id}}">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title">Are you sure to delete this store?</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body" style="text-align: right;">
                                  <form method="POST" action="{{ route('store.destroy',$store->id) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-danger">Delete</button>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal -->
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
    </div>
</section>
@endsection